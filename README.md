## blockade
A bash script for managing ad-block list via hosts file
```
Usage:
 blockade add [domain]     add a domain to the ad-block list
 blockade delete [domain]  remove a domain from the ad-block list
 blockade list             show all entries in the ad-block list
 blockade count            show entry count
 blockade help             show this message and exit
```
All entries are added to /etc/hosts.ads file that must be created before using script
